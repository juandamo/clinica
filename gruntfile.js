module.exports = function(grunt) {

  // Project configuration.

  // Minificar archivos CSS
  grunt.initConfig({
  	cssmin: {
 	 target: {
    files: [{
      expand: true,
      cwd: 'css',
      src: ['*.css', '!*.min.css'],
      dest: 'css',
      ext: '.min.css'
    }]
  }
},

// Minificar archivos js
  uglify: {
	    my_target: {
	      files: [{
	        expand: true,
	        cwd: 'js',
	        src: ['**/*.js', '!*.min.js'],
	        dest: 'js',
	        ext: '.min.js'
	      }]
	    }
	  },

    // Minificar html

	htmlmin: {                                     // Task
    dist: {                                      // Target
      options: {                                 // Target options
        removeComments: true,
        collapseWhitespace: true
      },
      files: {                                   // Dictionary of files
        'index.html': 'index.html',     // 'destination': 'source'
        // 'dist/contact.html': 'src/contact.html'
      }
    },
    dev: {                                       // Another target
        files: [{
          expand: true,
          cwd: '',
          src: ['/**/*.html'],
          dest: ''
      }]
    }
  }



  })

  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-htmlmin');
  grunt.registerTask('default', ['cssmin','uglify','htmlmin']);



};