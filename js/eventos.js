var citas = [];
var usuarios = [];

function Registrar() {
	var nombres, textApellidos, textIdentificacion, textCorreo, passwordinput, usuario;
	usuario = {};
	nombres = document.getElementById("Nombres").value;
	apellidos = document.getElementById("textApellidos").value;
	identificacion = document.getElementById("textIdentificacion").value;
	correo = document.getElementById("textCorreo").value;
	contraseña = document.getElementById("passwordinput").value;
	passwordinputRepeat = document.getElementById("passwordinputRepeat").value;


	usuario.nombres = nombres;
	usuario.apellidos = apellidos;
	usuario.identificacion = identificacion;
	usuario.correo = correo;
	usuario.contraseña = contraseña;

	// localStorage.setItem("usuario", JSON.stringify(usuario));
	usuarios.push(usuario);
	localStorage.setItem("usuarios", JSON.stringify(usuarios));

	alert("Usuario registrado correctamente");
	mostrarInicio();
}



function PedirCita() {
	console.log("ingreso en pedir cita")
	var mes, dia, especialidad, cita;
	cita = {};
	mes = document.getElementById("seleccionarMes").value;
	dia = document.getElementById("seleccionarDia").value;
	especialidad = document.getElementById("selectEspecialidad").value;

	cita.mes = mes;
	cita.dia = dia;
	cita.especialidad = especialidad;

	citas.push(cita);
	localStorage.setItem("citas", JSON.stringify(citas));
	alert("La cita se ha asignado exitosamente");
}

function IniciarSesion() {

    // document.getElementById("result").innerHTML = localStorage.getItem("usuarios");
	if (JSON.parse(localStorage.getItem("usuarios"))!=null){
		usuarios= JSON.parse(localStorage.getItem("usuarios"));
	}
	
	var id, password, login, logueado;
	logueado= false;
	id = document.getElementById("textinputId").value;
	password = document.getElementById("passwordinput").value;

	if(usuarios.length==0){
		alert("El arreglo usuario en el localStorage no se cargó correctamente o aún no se registraron usuarios")
	}else{
		for (i= 0; i<usuarios.length;i++){
				if (usuarios[i].identificacion == id) {
					if (usuarios[i].contraseña == password) {
						logueado= true;
						localStorage.setItem("logueado", logueado);
						document.getElementById("btn_agendar_cita").classList.remove("ocultar");
						// document.getElementById("btn_mostrar_citas").classList.remove("ocultar");
						// document.getElementById("btn_mostrar_diagnosticos").classList.remove("ocultar");
						document.getElementById("button2id").classList.add("ocultar");
						document.getElementById("button1id").innerHTML = "Cerrar Sesión";
						alert("Se ha iniciado sesión");
						mostrarInicio();
						break;
					} else {
						localStorage.setItem("logueado", logueado);
						alert("La contraseña es incorrecta");
						break;
					}
				}
				if((i==usuarios.length-1) && (logueado!=true)){
					alert("El usuario no se encuentra registrado");
				}
		}
	}
}