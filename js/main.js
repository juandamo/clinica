function mostrarLogin() {
    var login = JSON.parse(localStorage.getItem('logueado'));

    if (login == false) {
        var xhr = typeof XMLHttpRequest != 'undefined' ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
        xhr.open('get', 'views/login.html', true);
        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4 && xhr.status == 200) {
                document.getElementById('contenido').innerHTML = xhr.responseText;
            }
        }
        xhr.send();
        document.getElementById('contenido').innerHTML = 'views/login.html'
    } else {
        localStorage.setItem("logueado", false);
        document.getElementById("btn_agendar_cita").classList.add("ocultar");
        // document.getElementById("btn_mostrar_citas").classList.add("ocultar");
        // document.getElementById("btn_mostrar_diagnosticos").classList.add("ocultar");
        document.getElementById("button2id").classList.remove("ocultar");
        document.getElementById("button1id").innerHTML = "Iniciar Sesión";
        mostrarInicio();
    }
}

function mostrarRegistro() {
    var xhr = typeof XMLHttpRequest != 'undefined' ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
    xhr.open('get', 'views/registro.html', true);
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
            document.getElementById('contenido').innerHTML = xhr.responseText;
        }
    }
    xhr.send();
    document.getElementById('contenido').innerHTML = 'views/registro.html'
}

function mostrarAgendar() {

    var xhr = typeof XMLHttpRequest != 'undefined' ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
    xhr.open('get', 'views/agendar.html', true);
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
            document.getElementById('contenido').innerHTML = xhr.responseText;
        }
    }
    xhr.send();
    document.getElementById('contenido').innerHTML = 'views/agendar.html'
}

function mostrarContacto() {
    var xhr = typeof XMLHttpRequest != 'undefined' ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
    xhr.open('get', 'views/contactos.html', true);
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
            document.getElementById('contenido').innerHTML = xhr.responseText;
        }
    }
    xhr.send();
    document.getElementById('contenido').innerHTML = 'views/contactos.html'
}

// function mostrarCitas() {
//     var xhr = typeof XMLHttpRequest != 'undefined' ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
//     xhr.open('get', 'views/citas.html', true);
//     xhr.onreadystatechange = function () {
//         if (xhr.readyState == 4 && xhr.status == 200) {
//             document.getElementById('contenido').innerHTML = xhr.responseText;
//         }
//     }
//     xhr.send();
//     document.getElementById('contenido').innerHTML = 'views/citas.html'
// }

// function mostrarDiagnosticos() {
//     var xhr = typeof XMLHttpRequest != 'undefined' ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
//     xhr.open('get', 'views/diagnosticos.html', true);
//     xhr.onreadystatechange = function () {
//         if (xhr.readyState == 4 && xhr.status == 200) {
//             document.getElementById('contenido').innerHTML = xhr.responseText;
//         }
//     }
//     xhr.send();
//     document.getElementById('contenido').innerHTML = 'views/diagnosticos.html'
// }

function mostrarInicio() {
    var xhr = typeof XMLHttpRequest != 'undefined' ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
    xhr.open('get', 'views/inicio.html', true);
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
            document.getElementById('contenido').innerHTML = xhr.responseText;
        }
    }
    xhr.send();
    document.getElementById('contenido').innerHTML = 'views/inicio.html'

}

window.onload = function () {
    mostrarInicio();
}